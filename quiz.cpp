1.Both while and do-while loop are the iteration statement, if we want that 
first, the condition should be verified, and then the statements inside 
the loop must execute, then the while loop is used. If you want to test the 
termination condition at the end of the loop, then the do-while loop is used.

Some difference between while and do-while:
while:
1.Condition is checked first then statement(s) is executed.
2.If there is a single statement, brackets are not required.	
3.It might occur statement(s) is executed zero times, If condition is false.

do-while:
1.Statement(s) is executed atleast once, thereafter condition is checked.
2.Brackets are always required.
3.At least once the statement(s) is executed.



2.A
3.Prints from one to five
4.Prints from zero to four
5.Output will be from zero to four, without 3. (0, 1, 2, 4)
6.Prints from zero to two
7.#include <iostream>

using namespace std;

int main()
{
    
 
 int a,b;
 cin>>a>>b;
 for(int i = a; i < b; i++){
     if(i%2 == 0)
     cout<<i<<" ";
 }
 
 return 0;

}

8.int main()
{
    int arr[100];
    int n,a;
    cin>>n;
    
    for(int i = 0;i < n;i++){
        cin>>arr[i];
    }
    
    for(int i = 0;i < n;i++){
        if(arr[i] == 0)
            cout<<"Yes";
         else
        cout<<"No";
    }
    
    
 
 return 0;


9.int main()
{
    int arr[100];
    int n;
    cin>>n;
    

    for(int i = 0;i < n;i++){
        cin>>arr[i];
    }
    
  
    int sum = 0;
    for(int i = 0;i < n;i++){
        sum=sum + arr[i];
    }
    cout<<sum;
 
 return 0;

}